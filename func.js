$(function () {
    //监听游戏规则的显示和隐藏
    $('.rules').click(function () {
        $('.rule').stop().fadeIn(100);

        $('.rule a').click(function () {
            $('.rule').stop().fadeOut(100);
        })
    });

    //监听开始按钮
    $('.start').click(function () {
        $(this).stop().fadeOut(100);

        //进度条减少
        progressHander();
        //处理灰太狼小灰灰动画
        wolfAnimation();
    });

    //监听重新开始按钮
    $('.mask button').click(function () {
        $('.mask').stop().fadeOut(100);
        progressHander();
        wolfAnimation();

        //得分清空
        $('.count').text(0);
    });

    //游戏结束停止动画
    function stopWolfAnimation() {
        $('.wolfimg').remove();
        clearInterval(wolfTimer);
    }

    //小灰灰灰太狼动画
    var wolfTimer = null;
    function wolfAnimation() {
        // 1.定义两个数组保存所有灰太狼和小灰灰的图片
        var wolf_1 = ['./images/h0.png', './images/h1.png', './images/h2.png', './images/h3.png', './images/h4.png',
            './images/h5.png', './images/h6.png', './images/h7.png', './images/h8.png', './images/h9.png'];
        var wolf_2 = ['./images/x0.png', './images/x1.png', './images/x2.png', './images/x3.png', './images/x4.png',
            './images/x5.png', './images/x6.png', './images/x7.png', './images/x8.png', './images/x9.png'];
        // 2.定义一个数组保存所有可能出现的位置
        var arrPos = [
            { left: "100px", top: "115px" },
            { left: "20px", top: "160px" },
            { left: "190px", top: "142px" },
            { left: "105px", top: "193px" },
            { left: "19px", top: "221px" },
            { left: "202px", top: "212px" },
            { left: "120px", top: "275px" },
            { left: "30px", top: "295px" },
            { left: "209px", top: "297px" }
        ];
        //创建一个图片对象
        var oImg = $("<img src='' class='wolfimg'>");

        //设置图片位置
        var index = Math.round(Math.random() * (8 - 0) + 0);
        $(oImg).css({
            position: 'absolute',
            left: arrPos[index].left,
            top: arrPos[index].top
        });
        //设置图片内容,随机获取灰太狼小灰灰
        var wolf = Math.round(Math.random() * (1 - 0) + 0) == 0 ? wolf_1 : wolf_2;
        //图片动画
        var wolfindex = 0;
        var wolftarget = 5;
        wolfTimer = setInterval(function () {
            //判断是否是拍打图片wolfindex>5是
            if (wolfindex > wolftarget) {
                oImg.remove();
                clearInterval(wolfTimer);
                wolfAnimation();
            }
            $(oImg).attr('src', wolf[wolfindex]);
            wolfindex++;
        }, 300)

        //添加图片
        $('.container').append(oImg);


        //拍打动画
        $(oImg).one('click', function () {
            //拍打动画
            wolfindex = 6;
            wolftarget = 9;

            //判断打的是小灰灰还是灰太狼,计算分数
            var oSrc = $(this).attr('src');
            var flag = oSrc.indexOf('h') > 0;//true灰太狼
            if (flag) {
                $('.container .count').text(parseInt($('.container .count').text()) + 10);
            } else {
                $('.container .count').text(parseInt($('.container .count').text()) - 10);
            }

        })
    }

    function progressHander() {
        //重新设置进度条
        var proWidth = $('.progress').width(180);

        var timer = setInterval(function () {
            proWidth = $('.progress').width();
            proWidth -= 6;
            $('.progress').css("width", proWidth);

            if (proWidth <= 0) {
                clearInterval(timer);
                //显示游戏结束
                $('.mask').stop().fadeIn(100);
                //结束wolfAnimation
                stopWolfAnimation();

            }
        }, 1000)

    }
})